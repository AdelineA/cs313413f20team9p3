package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import java.util.ArrayList;
import java.util.List;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {


	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c)
	{
		final int tempColor = paint.getColor();//hold OG color
		paint.setColor(c.getColor());//set color to specified color
		// visitor moves through the decorators
		c.getShape().accept(this);
		paint.setColor(tempColor);//revert color back to OG
		return null;
	}

	@Override
	public Void onFill(final Fill f)
	{
		//set the style of paint to fill the shape
		paint.setStyle(Style.FILL_AND_STROKE);
		//visitor moves through the decorators
		f.getShape().accept(this);
		paint.setStyle(Style.STROKE);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
	for(Shape s: g.getShapes())
		{
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
	    canvas.translate(-l.getX(),-l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0 , 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		final Style tmp = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(tmp);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		final float[] pts = new float[(4*s.getPoints().size())];
		int i = 0;
		for (int k = 0; k < s.getPoints().size(); k++) {
			Point p = s.getPoints().get(k);
			if (i >= 2) { 						//checks to see if it's first point
				pts[i] = p.getX();
				pts[i + 1] = p.getY();
				pts[i + 2] = p.getX();
				pts[i + 3] = p.getY();
				i = i + 4;
			} else {
				pts[i] = p.getX();				//add first Point x and y locations to beginning
				pts[i + 1] = p.getY();
				i = i + 2;
			}
		}
		pts[i] = pts[0];						//add first Point x and y locations to end
		pts[i + 1] = pts[1];
		canvas.drawLines(pts, paint);
		return null;
	}
}
