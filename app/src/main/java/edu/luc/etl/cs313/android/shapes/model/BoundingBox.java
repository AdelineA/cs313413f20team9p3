package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Rect;

import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f)
	{
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {

		int xMin = Integer.MAX_VALUE;
		int xMax = Integer.MIN_VALUE;
		int yMin = Integer.MAX_VALUE;
		int yMax = Integer.MIN_VALUE;

		for (Shape shape : g.getShapes()) {
			final Location boundingBox = shape.accept(this);
			final Rectangle rect = (Rectangle) boundingBox.getShape();

			xMin = Math.min(xMin, boundingBox.getX());
			xMax = Math.max(xMax, boundingBox.getX() + rect.getWidth());
			yMin = Math.min(yMin, boundingBox.getY());
			yMax = Math.max(yMax, boundingBox.getY() + rect.getHeight());
		}

		return new Location(xMin, yMin, new Rectangle(xMax - xMin, yMax - yMin));
	}

	@Override
	public Location onLocation(final Location l)
	{
	final int x = l.getX();//x of the original location
	final int y = l.getY();
	final Location newLocation = l.getShape().accept(this);//location of the bounding box of location's child shape
	final int nX = newLocation.getX();
	final int nY = newLocation.getY();
	return new Location((x + nX), (y + nY), newLocation.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r)
	{
		final int x = r.getWidth();
		final int y = r.getHeight();
		return new Location(0,0, new Rectangle(x,y));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c)
	{
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o)
	{
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s)
	{
		int xMin = Integer.MAX_VALUE;
		int xMax = Integer.MIN_VALUE;
		int yMin = Integer.MAX_VALUE;
		int yMax = Integer.MIN_VALUE;

		for (Point point : s.getPoints()) {
			xMin = Math.min(xMin, point.accept(this).getX());
			xMax = Math.max(xMax, point.accept(this).getX());
			yMin = Math.min(yMin, point.accept(this).getY());
			yMax = Math.max(yMax, point.accept(this).getY());
		}

		return new Location(xMin, yMin, new Rectangle((xMax - xMin), (yMax - yMin)));
	}
}
